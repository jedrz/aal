#ifndef _IO_H_
#define _IO_H_

#include <iostream>
#include <vector>
#include <functional>

#include "common.h"

class CmdParser
{
public:
    CmdParser(char*[], int);
    bool optionExists(const std::string&);
    std::string getOption(const std::string&);
private:
    std::vector<std::string> args;
};

std::vector<Point> generatePoints(int, int, int, unsigned);
std::vector<Point> handleInput(std::istream&);

class Q
{
public:
    Q(double, int, std::function<double(int)>);
    double operator()(double, int);
private:
    std::function<double(int)> T;
    double c;
};

void printSequence(const std::vector<Point>&);
std::vector<int> strToInts(const std::string&);
void timeMeasure(std::vector<int>,
                 std::function<Result(std::vector<Point>)>,
                 std::function<double(int)>,
                 int, int, int);

#endif
