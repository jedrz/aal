#include "io.h"

#include <ctime>
#include <cstdlib>
#include <algorithm>
#include <iomanip>

CmdParser::CmdParser(char* argv[], int argc)
{
    for (int i = 0; i < argc; ++i) {
        char* arg = argv[i];
        std::string s = arg;
        args.push_back(s);
    }
}

bool CmdParser::optionExists(const std::string& option)
{
    return std::find(args.begin(), args.end(), option) != args.end();
}

// Zwraca argument występujący po podanej opcji.
std::string CmdParser::getOption(const std::string& option)
{
    auto it = std::find(args.begin(), args.end(), option);
    if (it != args.end() && ++it != args.end()) {
        return *it;
    }
    return std::string();
}

bool addWithoutDuplicates(std::vector<Point>& points, Point point)
{
    for (const Point& p : points) {
        if (p.getX() == point.getX() && p.getY() == point.getY()) {
            return false;
        }
    }
    points.push_back(point);
    return true;
}

// Generuje punkty o zadanych parametrach bez powtórzeń.
std::vector<Point> generatePoints(int maxX, int maxY, int maxWeight, unsigned n)
{
    std::srand(std::time(NULL));
    std::vector<Point> points;
    while (points.size() < n) {
        int x = rand() % (maxX + 1);
        int y = rand() % (maxY + 1);
        int w = rand() % maxWeight + 1;
        Point point(x, y, w);
        addWithoutDuplicates(points, point);
    }
    return points;
}

// Wczytuje punkty ze strumienia ignorując powtórzenia.
std::vector<Point> handleInput(std::istream& in)
{
    std::vector<Point> points;
    int x, y, w;
    while (in >> x >> y >> w) {
        Point point(x, y, w);
        addWithoutDuplicates(points, point);
    }
    return points;
}

// Implementacja funkcji q z instrukcji.
// t - czas wykonania algorytmu dla mediany zestawu problemów
// n - rozmiar tego problemu
// T - ocena teoretyczna złożoności czasowej
Q::Q(double t, int n, std::function<double(int)> T)
    : T(T), c(t / T(n))
{
}

// Zwraca wartość funkcji dla podanego czasu wykonania i rozmiaru problemu.
// q > 1 oznacza, że wystąpiło przeszacowanie
// q < 1 niedoszacowanie
double Q::operator()(double t, int n)
{
    return t / (c * T(n));
}

void printSequence(const std::vector<Point>& seq)
{
    for (const Point& p : seq) {
        std::cout << p.getX() << " " << p.getY() << " " << p.getWeight() << std::endl;
    }
}

// Zamienia napis zawierający liczby oddzielone przecinkami na wektor liczb.
std::vector<int> strToInts(const std::string& strSeq)
{
    std::vector<int> numbers;

    size_t commaPos = strSeq.find(',');
    size_t beg = 0;
    while (commaPos != std::string::npos) {
        size_t end = commaPos;
        int n = std::atoi(strSeq.substr(beg, end).c_str());
        numbers.push_back(n);
        beg = commaPos + 1;
        commaPos = strSeq.find(',', commaPos + 1);
    }
    int n = std::atoi(strSeq.substr(beg).c_str());
    numbers.push_back(n);

    return numbers;
}

// Wykonuje pomiar czasu oraz wartość funkcji q dla:
// podanego zestawu wielkości problemu,
// funkcji rozwiązującej problem,
// funkcji zwracającej złożoność problemu dla zadanej wielkości.
// Generowane punkty zawierają się w podanym zakresie.
void timeMeasure(std::vector<int> nVec,
                 std::function<Result(std::vector<Point>)> solveFn,
                 std::function<double(int)> TFn,
                 int maxX, int maxY, int maxWeight)
{
    std::sort(nVec.begin(), nVec.end());

    int med = nVec.size() / 2;
    std::vector<Point> points = generatePoints(maxX, maxY, maxWeight, nVec[med]);
    std::clock_t t = std::clock();
    solveFn(points);
    t = std::clock() - t;
    Q q(t, nVec[med], TFn);

    std::cout << std::setw(6) << "n"
              << std::setw(10) << "t [ms]"
              << std::setw(8) << "q(n)" << std::endl;

    for (int n : nVec) {
        std::vector<Point> points = generatePoints(maxX, maxY, maxWeight, n);
        clock_t t = std::clock();
        solveFn(points);
        t = std::clock() - t;
        double tMS = t * 1000.0 / CLOCKS_PER_SEC;
        std::cout << std::fixed;
        std::cout << std::setw(6) << n << " "
                  << std::setw(10) << std::setprecision(5) << tMS << " "
                  << std::setw(8) << std::setprecision(5) << q(t, n) << std::endl;
    }
}
