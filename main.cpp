#include <iostream>
#include <functional>
#include <algorithm>
#include <cassert>
#include <ctime>
#include <cmath>

#include "algorithm1.h"
#include "algorithm2.h"
#include "algorithm3.h"
#include "io.h"

std::string helpText =
"-h                     wyświetla pomoc\n"
"\n"
"-1                     używa algorytmu o złożoności O(n logn)\n"
"-2                     używa algorytmu o złożoności O(n^2)\n"
"-3                     używa algorytmu o złożoności O(N^2)\n"
"\n"
"-i                     wczytanie punktów ze standardowego wejścia w formacie: x y waga\n"
"\n"
"-g                     generuje punkty\n"
"-x liczba              zakres generowanych wartości x\n"
"-y liczba              zakres generowanych wartości y\n"
"-w liczba              zakres generowanych wartości wag\n"
"-n liczba              liczba generowanych punktów\n"
"\n"
"-t                     mierzy czas oraz sprawdza przewidywaną złożoność algorytmu\n"
"                       dla podanych wielkości problemu\n"
"-n liczba1,liczba2,... wielkości problemu oddzielone przecinkami, bez spacji\n"
"\n"
"-c                     sprawdza poprawność algorytmów, poprzez zbadanie\n"
"                       zwróconych sum wag\n"
"-ps                    drukuje dodatkowo znalezioną sekwencję\n"
"-2t                    znajduje ścieżkę niespełniającą warunku:\n"
"                       \"każdy wektor z wyjątkiem co najwyżej jednego pomiędzy\n"
"                       kolejnymi punktami z ciągu ma nieujemne składowe.\"\n";

int main(int argc, char *argv[])
{
    CmdParser parser(argv, argc);

    // Wektor punktów dla którego należy znaleźć najcięższą sekwencję.
    std::vector<Point> points;

    // Wektor rozmiarów problemu, dla których zostanie przeprowadzony pomiar
    // czasu oraz badanie złożoności.
    std::vector<int> nVec;

    if (parser.optionExists("-h")) {
        std::cout << helpText;
        exit(0);
    }

    // Zakresy wartości.
    int x = std::atoi(parser.getOption("-x").c_str());
    if (x == 0) {
        x = 1000;
    }
    int y = std::atoi(parser.getOption("-y").c_str());
    if (y == 0) {
        y = 1000;
    }
    int w = std::atoi(parser.getOption("-w").c_str());
    if (w == 0) {
        w = 1000;
    }

    if (parser.optionExists("-i")) {
        points = handleInput(std::cin);
    } else if (parser.optionExists("-t")) {
        std::string nSeq = parser.getOption("-n");
        nVec = strToInts(nSeq);
    } else { //if (parser.optionExists("-g"))
        int n = std::atoi(parser.getOption("-n").c_str());
        if (n == 0) {
            n = 10000;
        }
        points = generatePoints(x, y, w, n);
    }

    bool ifPrintSequence = parser.optionExists("-ps");
    bool solve2Too = parser.optionExists("-2t");

    if (!nVec.empty()) {
        std::function<Result(std::vector<Point>)> solveFn;
        std::function<double(int)> TFn;
        if (parser.optionExists("-2")) {
            solveFn = Algorithm2::solve1;
            TFn = [](int n) { return n * n; };
        } else {
            solveFn = Algorithm1::solve1;
            TFn = [](int n) { return n * std::log2(n); };
        }
        timeMeasure(nVec,
                    solveFn,
                    TFn,
                    x, y, w);
    } else if (parser.optionExists("-c")) {
        std::vector<std::function<Result(std::vector<Point>)>> solve1Fns {
            Algorithm1::solve1,
            Algorithm2::solve1,
            Algorithm3::solve1
        };
        std::vector<std::function<Result(std::vector<Point>)>> solve2Fns {
            Algorithm1::solve2,
            Algorithm2::solve2,
            Algorithm3::solve2,
        };
        std::vector<Result> results;
        for (auto fn : solve1Fns) {
            Result r = fn(points);
            std::cout << r.getSumOfWeights() << std::endl;
            results.push_back(r);
        }
        for (auto r1 : results) {
            for (auto r2 : results) {
                assert(r1.getSumOfWeights() == r2.getSumOfWeights());
            }
        }
        results.clear();
        for (auto fn : solve2Fns) {
            Result r = fn(points);
            std::cout << r.getSumOfWeights() << std::endl;
            results.push_back(r);
        }
        for (auto r1 : results) {
            for (auto r2 : results) {
                assert(r1.getSumOfWeights() == r2.getSumOfWeights());
            }
        }
    } else {
        std::function<Result(std::vector<Point>)> solve1Fn;
        std::function<Result(std::vector<Point>)> solve2Fn;
        if (parser.optionExists("-3")) {
            solve1Fn = Algorithm3::solve1;
            solve2Fn = Algorithm3::solve2;
        } else if (parser.optionExists("-2")) {
            solve1Fn = Algorithm2::solve1;
            solve2Fn = Algorithm2::solve2;
        } else {
            solve1Fn = Algorithm1::solve1;
            solve2Fn = Algorithm1::solve2;
        }
        Result result = solve1Fn(points);
        std::cout << result.getSumOfWeights() << std::endl;
        if (ifPrintSequence) {
            printSequence(result.getSequence());
        }
        if (solve2Too) {
            result = solve2Fn(points);
            std::cout << result.getSumOfWeights() << std::endl;
            if (ifPrintSequence) {
                printSequence(result.getSequence());
            }
        }
    }

    return 0;
}
