#ifndef _ALGORITHM1_H_
#define _ALGORITHM1_H_

#include "common.h"

namespace Algorithm1
{
    Result solve1(std::vector<Point>);
    Result solve2(std::vector<Point>);
}

#endif
