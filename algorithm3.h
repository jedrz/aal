#ifndef _ALGORITHM3_H_
#define _ALGORITHM3_H_

#include <vector>

#include "common.h"

namespace Algorithm3
{
    Result solve1(std::vector<Point>);
    Result solve2(std::vector<Point>);
}

#endif
