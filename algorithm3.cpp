#include "algorithm3.h"

#include <algorithm>

// Pomocnicza struktura dla algorytmu.
struct PointsMap
{
    struct XY
    {
        int x, y;
        XY(int x = -1, int y = -1) : x(x), y(y)
        {}
    };

    PointsMap(const std::vector<Point>&);
    void reset(const std::vector<Point>&);

    // Wagi punktów.
    std::vector<std::vector<int>> points;

    // Maksymalna waga sekwencji kończącej się w pewnym punkcie.
    std::vector<std::vector<int>> weights;

    // Współrzędne poprzednika punktu.
    std::vector<std::vector<XY>> prev;

    // Maksymalny zarejestrowany x, y.
    int destX, destY;
};

PointsMap::PointsMap(const std::vector<Point>& pointsVec)
    : destX(0), destY(0)
{
    for (const Point& p : pointsVec) {
        destX = std::max(destX, p.getX());
        destY = std::max(destY, p.getY());
    }

    points.resize(destX + 1, std::vector<int>(destY + 1, 0));
    weights.resize(destX + 1, std::vector<int>(destY + 1, 0));
    prev.resize(destX + 1, std::vector<XY>(destY + 1));

    for (const Point& p : pointsVec) {
        int x = p.getX();
        int y = p.getY();
        int w = p.getWeight();
        points[x][y] = w;
    }
}

// Usuwa użyte punkty oraz resetuje wektor wag i poprzedników.
void PointsMap::reset(const std::vector<Point>& seq)
{
    for (const Point& p : seq) {
        int x = p.getX();
        int y = p.getY();
        points[x][y] = 0;
    }

    weights.resize(destX + 1, std::vector<int>(destY + 1, 0));
    prev.resize(destX + 1, std::vector<XY>(destY + 1));
}

Result solve(PointsMap& pm)
{
    // Wypełnienie przypadków brzegowych.
    pm.weights[0][0] = pm.points[0][0];

    // Wypełnienie wag i poprzedników na osi X.
    for (int x = 1; x <= pm.destX; ++x) {
        pm.weights[x][0] = pm.weights[x - 1][0] + pm.points[x][0];
        if (pm.points[x - 1][0] > 0) {
            pm.prev[x][0] = PointsMap::XY(x - 1, 0);
        } else {
            pm.prev[x][0] = pm.prev[x - 1][0];
        }
    }

    // Wypełnienie wag i poprzedników na osi Y.
    for (int y = 1; y <= pm.destY; ++y) {
        pm.weights[0][y] = pm.weights[0][y - 1] + pm.points[0][y];
        if (pm.points[0][y - 1] > 0) {
            pm.prev[0][y] = PointsMap::XY(0, y - 1);
        } else {
            pm.prev[0][y] = pm.prev[0][y - 1];
        }
    }

    // Wypełnienie pozostałych przypadków.
    for (int x = 1; x <= pm.destX; ++x) {
        for (int y = 1; y <= pm.destY; ++y) {
            if (pm.weights[x][y - 1] > pm.weights[x - 1][y]) {
                pm.weights[x][y] = pm.weights[x][y - 1] + pm.points[x][y];
                if (pm.points[x][y - 1] > 0) {
                    pm.prev[x][y] = PointsMap::XY(x, y - 1);
                } else {
                    pm.prev[x][y] = pm.prev[x][y - 1];
                }
            } else {
                pm.weights[x][y] = pm.weights[x - 1][y] + pm.points[x][y];
                if (pm.points[x - 1][y] > 0) {
                    pm.prev[x][y] = PointsMap::XY(x - 1, y);
                } else {
                    pm.prev[x][y] = pm.prev[x - 1][y];
                }
            }
        }
    }

    std::vector<Point> sequence;
    int x = pm.destX;
    int y = pm.destY;
    // Znalezienie końca sekwencji.
    if (pm.points[x][y] == 0) {
        int tmpX = pm.prev[x][y].x;
        int tmpY = pm.prev[x][y].y;
        x = tmpX;
        y = tmpY;
    }
    // Dodawanie punktów do momentu, gdy dany punkt nie ma poprzednika.
    while (x != -1 || y != -1) {
        sequence.push_back(Point(x, y, pm.points[x][y]));
        int tmpX = pm.prev[x][y].x;
        int tmpY = pm.prev[x][y].y;
        x = tmpX;
        y = tmpY;
    }
    reverse(sequence.begin(), sequence.end());
    return Result(sequence, pm.weights[pm.destX][pm.destY]);
}

Result Algorithm3::solve1(std::vector<Point> points)
{
    PointsMap pm(points);
    Result result = solve(pm);
    return result;
}

Result Algorithm3::solve2(std::vector<Point> points)
{
    PointsMap pm(points);
    Result firstResult = solve(pm);
    pm.reset(firstResult.getSequence());
    Result secondResult = solve(pm);

    std::vector<Point>& sequence = firstResult.getSequence();
    sequence.insert(sequence.end(), secondResult.getSequence().begin(),
                    secondResult.getSequence().end());
    return Result(
        sequence, firstResult.getSumOfWeights() + secondResult.getSumOfWeights());
}
