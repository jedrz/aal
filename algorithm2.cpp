#include "algorithm2.h"

#include <algorithm>

Result Algorithm2::solve1(std::vector<Point> points)
{
    sort(points);
    // Najlepsza suma wag dla ciągu konczącego się i-tym elementem
    std::vector<int> his(points.size());
    // Zawiera indeks poprzednika.
    std::vector<int> prev(points.size(), -1);

    for (unsigned i = 0; i < points.size(); ++i) {
        his[i] = points[i].getWeight();
    }

    for (unsigned i = 0; i < points.size(); ++i) {
        int vi = points[i].getY();
        int wi = points[i].getWeight();
        for (unsigned j = 0; j < i; ++j) {
            int vj = points[j].getY();
            // Czy da się j-tym elementem rozszerzyć sekwencję kończącą się
            // i-tym elementem oraz czy waga takiej sekwencji będzie większa od
            // dotychczas obliczonej.
            if (vi >= vj && his[j] + wi > his[i]) {
                his[i] = his[j] + wi;
                prev[i] = j;
            }
        }
    }

    auto maxHisIt = std::max_element(his.begin(), his.end());
    int maxHis = *maxHisIt;

    std::vector<Point> sequence;
    for (int i = maxHisIt - his.begin(); i != -1; i = prev[i]) {
        sequence.push_back(points[i]);
    }
    std::reverse(sequence.begin(), sequence.end());

    return Result(sequence, maxHis);
}

// Algorytm jak wyżej, z tym, że pomija punkty zaznaczone w wektorze, a
// następnie zaznacza jakie punkty zostały użyte w znalezionej sekwencji.
static Result solve2Phase(const std::vector<Point>& points,
                          std::vector<bool>& used)
{
    std::vector<int> his(points.size());
    std::vector<int> prev(points.size(), -1);

    for (unsigned i = 0; i < points.size(); ++i) {
        his[i] = used[i] ? 0 : points[i].getWeight();
    }

    for (unsigned i = 0; i < points.size(); ++i) {
        if (!used[i]) {
            int vi = points[i].getY();
            int wi = points[i].getWeight();
            for (unsigned j = 0; j < i; ++j) {
                if (!used[j]) {
                    int vj = points[j].getY();
                    if (vi >= vj && his[j] + wi > his[i]) {
                        his[i] = his[j] + wi;
                        prev[i] = j;
                    }
                }
            }
        }
    }

    auto maxHisIt = std::max_element(his.begin(), his.end());
    int maxHis = *maxHisIt;

    // Obsłużenie pustej ścieżki.
    if (maxHis == 0) {
        return Result(std::vector<Point>(), 0);
    }

    std::vector<Point> sequence;
    for (int i = maxHisIt - his.begin(); i != -1; i = prev[i]) {
        sequence.push_back(points[i]);
        used[i] = true;
    }
    std::reverse(sequence.begin(), sequence.end());

    return Result(sequence, maxHis);
}

Result Algorithm2::solve2(std::vector<Point> points)
{
    sort(points);
    std::vector<bool> usedPoints(points.size(), false);
    Result r1 = solve2Phase(points, usedPoints);
    Result r2 = solve2Phase(points, usedPoints);
    std::vector<Point> sequence = r1.getSequence();
    sequence.insert(sequence.end(), r2.getSequence().begin(),
                    r2.getSequence().end());
    int sum = r1.getSumOfWeights() + r2.getSumOfWeights();
    return Result(sequence, sum);
}
