#include "algorithm1.h"

#include <set>
#include <algorithm>

struct Key
{
    int v, w, index, prev;

    Key(int v, int w = 0, int index = -1, int prev = -1)
        : v(v), w(w), index(index), prev(prev)
    {}

    bool operator<(const Key& key) const
    {
        return v < key.v;
    }
};

typedef std::set<Key, std::less<Key>> Tree;

Tree::iterator findPreviousOrEqual(const Tree& tree, const Key& key)
{
    // Zwraca pierwszy, większy element od podanego.
    auto it = tree.upper_bound(key);

    // Niepotrzebny.
    // if (it == tree.end() && tree.empty()) {
    //     return it;
    //}
    if (it != tree.end() && key.v == it->v) {
        // Zwróć równy element podanemu.
        return it;
    } else if (it == tree.begin()) {
        // Zwróć koniec jeśli większy element od podanego jest pierwszy.
        // --begin nie przechodzi do end.
        return tree.end();
    }

    --it;
    return it;
}

Result Algorithm1::solve1(std::vector<Point> points)
{
    sort(points);

    Tree tree;

    // i-ty element wektora to indeks element poprzedzającego.
    std::vector<int> prev(points.size(), -1);

    for (unsigned i = 0; i < points.size(); ++i) {
        const Point& p = points[i];
        int v = p.getY();
        int w = p.getWeight();

        auto it = findPreviousOrEqual(tree, Key(v));

        int seqWeight = w;
        // Rozszerz sekwencję kończącą się mniejszym lub równym elementem.
        if (it != tree.end()) {
            seqWeight += it->w;
            prev[i] = it->index;
        } else {
            prev[i] = -1;
        }

        if (it != tree.end() && it->v < v) {
            // Zachowaj sekwencję kończącą się mniejszym elementem.
            // Mniejsze elementy o większej wadze od bieżącego mogą stanowić
            // najcięższą sekwencję.
            ++it;
        } else if (it == tree.end()) {
            // Nie ma mniejszych elementów od bieżącego, więc zacznij od
            // pierwszego większego.
            it = tree.begin();
        }

        // Usuń sekwencje, które kończą się większym elementem od bieżącego i
        // mają niewiększą wagę od nowej sekwencji.
        while (it != tree.end() && seqWeight >= it->w) {
            it = tree.erase(it);
        }

        tree.insert(Key(v, seqWeight, i, prev[i]));
    }

    auto end = --tree.end();
    int maxWeight = end->w;

    std::vector<Point> sequence;
    for (int i = end->index; i != -1; i = prev[i]) {
        sequence.push_back(points[i]);
    }

    std::reverse(sequence.begin(), sequence.end());

    return Result(sequence, maxWeight);
}

// Algorytm jak wyżej, z tym, że pomija punkty zaznaczone w wektorze, a
// następnie zaznacza jakie punkty zostały użyte w znalezionej sekwencji.
static Result solve2Phase(const std::vector<Point>& points,
                          std::vector<bool>& used)
{
    Tree tree;
    std::vector<int> prev(points.size(), -1);

    for (unsigned i = 0; i < points.size(); ++i) {
        if (used[i]) {
            continue;
        }
        const Point& p = points[i];
        int v = p.getY();
        int w = p.getWeight();

        auto it = findPreviousOrEqual(tree, Key(v));

        int seqWeight = w;
        if (it != tree.end()) {
            seqWeight += it->w;
            prev[i] = it->index;
        } else {
            prev[i] = -1;
        }

        if (it != tree.end() && it->v < v) {
            ++it;
        } else if (it == tree.end()) {
            it = tree.begin();
        }

        while (it != tree.end() && seqWeight >= it->w) {
            it = tree.erase(it);
        }

        tree.insert(Key(v, seqWeight, i, prev[i]));
    }

    // Obsłużenie pustej ścieżki.
    if (tree.empty()) {
        return Result(std::vector<Point>(), 0);
    }

    auto end = --tree.end();
    int maxWeight = end->w;

    std::vector<Point> sequence;
    for (int i = end->index; i != -1; i = prev[i]) {
        sequence.push_back(points[i]);
        used[i] = true;
    }

    std::reverse(sequence.begin(), sequence.end());

    return Result(sequence, maxWeight);
}

Result Algorithm1::solve2(std::vector<Point> points)
{
    sort(points);
    std::vector<bool> usedPoints(points.size(), false);
    Result r1 = solve2Phase(points, usedPoints);
    Result r2 = solve2Phase(points, usedPoints);
    std::vector<Point> sequence = r1.getSequence();
    sequence.insert(sequence.end(), r2.getSequence().begin(),
                    r2.getSequence().end());
    int sum = r1.getSumOfWeights() + r2.getSumOfWeights();
    return Result(sequence, sum);
}
