#ifndef _ALGORITHM2_H_
#define _ALGORITHM2_H_

#include "common.h"

namespace Algorithm2
{
    Result solve1(std::vector<Point>);
    Result solve2(std::vector<Point>);
}

#endif
