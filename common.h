#ifndef _COMMON_H_
#define _COMMON_H_

#include <vector>

class Point
{
public:
    Point(int, int, int);
    int getX() const;
    int getY() const;
    int getWeight() const;
private:
    int x, y, w;
};

class Result
{
public:
    Result(std::vector<Point>, int);
    std::vector<Point>& getSequence();
    int getSumOfWeights();
private:
    std::vector<Point> sequence;
    int sumOfWeights;
};

void sort(std::vector<Point>&);

#endif
