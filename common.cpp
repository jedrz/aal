#include "common.h"

#include <algorithm>

Point::Point(int x, int y, int w) : x(x), y(y), w(w)
{
}

int Point::getX() const
{
    return x;
}

int Point::getY() const
{
    return y;
}

int Point::getWeight() const
{
    return w;
}

Result::Result(std::vector<Point> sequence, int sumOfWeights)
    : sequence(sequence), sumOfWeights(sumOfWeights)
{
}

std::vector<Point>& Result::getSequence()
{
    return sequence;
}

int Result::getSumOfWeights()
{
    return sumOfWeights;
}

// Sortuje punkty najpierw po współrzędnej x oraz po współrzędnej y
// w ramach pewnego x.
void sort(std::vector<Point>& points)
{
    std::sort(points.begin(), points.end(),
              [](const Point& a, const Point& b) {
            return a.getX() < b.getX() ||
                   (a.getX() == b.getX() && a.getY() < b.getY());
    });
}

