CC=g++
CFLAGS=-c -Wall -Wextra -std=c++11
SRCS=main.cpp algorithm1.cpp algorithm2.cpp algorithm3.cpp common.cpp io.cpp
OBJS=$(SRCS:.cpp=.o)
TARGET=algorytm

all: $(TARGET)

$(TARGET): $(OBJS)
	$(CC) -o $(TARGET) $(OBJS)

.cpp.o:
	$(CC) $(CFLAGS) $< -o $@

.PHONY: clean

clean:
	rm -f *.o $(TARGET)
